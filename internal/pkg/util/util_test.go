package util

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestReverseInt(t *testing.T) {
	t.Run("Positive", positive)
	t.Run("String Instead Of Int", wrongType)
	t.Run("Negative Argument", negative)
	t.Run("Zero Argument", zero)
}

func ExampleReverseInt() {
	got, _ := ReverseInt(123)
	fmt.Println(got)
	// Output: 321
}

func positive(t *testing.T) {
	argument := 123
	expected := 321

	actual, _ := ReverseInt(argument)

	assert.Equal(t, expected, actual, "wrong reverse order for %d", argument)
}

func wrongType(t *testing.T) {
	argument := "123"
	expected := 0
	expectedErr := "not int"

	actual, actualErr := ReverseInt(argument)

	assert.Equal(t, expectedErr, actualErr.Error(), "wrong error message for %q", argument)
	assert.Equal(t, expected, actual, "wrong result on error for %q", argument)
}

func negative(t *testing.T) {
	argument := -649
	expected := -946

	actual, _ := ReverseInt(argument)

	assert.Equal(t, expected, actual, "wrong reverse order for %d", argument)
}

func zero(t *testing.T) {
	argument := 0
	expected := 0

	actual, _ := ReverseInt(argument)

	assert.Equal(t, expected, actual, "wrong reverse order for %d", argument)
}
